#!/usr/bin/env bash
ROOT=$(pwd)
cd test && terraform output -raw instance_ip_addr > ../ip
cd "$ROOT" || exit 0
server=$(<ip)

## Copy the html files to the server and configure files
ssh centos@"$server" "sudo rm -rf /var/www/netcentric.test"
ssh centos@"$server" "sudo mkdir -p /var/www/netcentric.test/public_html"
ssh centos@"$server" "sudo chown -R centos:centos /var/www/netcentric.test"
rsync -arzP demosite/ centos@"$server":/var/www/netcentric.test/public_html
ssh centos@"$server" "sudo chown -R centos:centos /var/www/netcentric.test"
ssh centos@"$server" "sudo chmod -R 755 /var/www/netcentric.test"

## Copy script files to the server
scp create-vhost.sh centos@"$server":/home/centos
# ssh centos@"$server" 'bash -s' < create-vhost.sh -v 2.4.37-41 -n netcentric.test

ssh centos@"$server"

