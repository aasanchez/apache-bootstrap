# Apache bootstrap

Create a Bash that does the following:

* Installs Apache 2.4 (not the latest, but a specific version that can be 4
  chosen as a variable or argument)
* Create a VHost and activate it
  * Include a 301 redirect inside the VHost
  * Include a Forbidden redirect
  * Include a regex redirect that catches all of the requests containing
  "career" and ".html" and redirects them to the /careers path
  * Include a PassThrough redirect
  * Include Basic Authentication (admin:123)
