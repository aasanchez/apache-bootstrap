#!/usr/bin/env bash
# shellcheck disable=SC2129

export NORMAL='\033[0m'
export RED='\033[1;31m'
export GREEN='\033[1;32m'
export YELLOW='\033[1;33m'
export WHITE='\033[1;37m'
export BLUE='\033[1;34m'
export PURPLE='\033[1;35m'

LOGFILE=~/create-vhost.log
VERBOSE="true"

if ! command -v yum &> /dev/null; then
  echo "This script is only for CentOS"
  exit 1
fi

# Force sudo execution
if [ $EUID != 0 ]; then sudo "$0" "$@"; exit $?; fi

while getopts ":hv:n:" optKey; do
	case "$optKey" in
		h|-*)
			help
			;;
		v)
			apache_version=$OPTARG
			;;
		n)
			server_name=$OPTARG
			;;
	esac
done

help() {
cat <<EOF
This script is a helper to create a new vhost for a new domain.
with some basic configuration.

Have been made for netcentric

Options:
  -v                  introduce the desire version for httpd
  -n                  define ServeName
  -h                  display help
EOF
	exit 1
}

logMessage() {
  scriptname=$(basename $0)
  if [ -f "$LOGFILE" ]; then
	echo "$(date +"%D %T") $scriptname : $*" >> $LOGFILE
  fi
}

message() {
  message="$*"
  echo -e "${WHITE} $message ${NORMAL}"
  logMessage "$message"
}

info() {
	message="$*"
  if [ "$VERBOSE" == "true" ]; then
    echo -e "${GREEN} $message "
    tput sgr0
    echo -e "${NORMAL}"
  fi
  logMessage "$message"
}

warning() {
	message="$*"
  if [ "$VERBOSE" == "true" ]; then
    echo -e "${YELLOW} $message"
    tput sgr0
    echo -e "${NORMAL}"
  fi
  logMessage "$message"
}

debug(){
	message="$*"
  if [ "$VERBOSE" == "true" ]; then
    echo -e "${BLUE} $message"
    tput sgr0
    echo -e "${NORMAL}"
  fi
  logMessage "$message"
}

error(){
  scriptname=$(basename "$0")
  message="$*"
  echo -e "${RED}"
	printf "%s\n"  "$scriptname $message" >&2;
  tput sgr0
  echo -e "${NORMAL}"
	logMessage "ERROR:  $message"
	return 1
}

valid_versions=("2.4.37-43" "2.4.37-41" "TBD" "TBD-2")

case $apache_version in
  "2.4.37-43")
    apache_version="2.4.37-43.module_el8.5.0+1022+b541f3b1"
    ;;
  "2.4.37-41")
    apache_version="2.4.37-41.module_el8.5.0+977+5653bbea"
    ;;
  *)
    apache_version="0000"
    ;;
esac

if [[ ${#apache_version} -lt 12 ]]; then
if [[ ! " ${valid_versions[*]} " =~ ${apache_version} ]]; then
  message "Invalid apache version. Valid versions are: ${valid_versions[*]}"
  PS3='Please enter your choice: '
  select opt in "${valid_versions[@]}"; do
    case $opt in
      "2.4.37-43")
        apache_version="2.4.37-43.module_el8.5.0+1022+b541f3b1"
        break
        ;;
      "2.4.37-41")
        apache_version="2.4.37-41.module_el8.5.0+977+5653bbea"
        break
        ;;
      "TBD")
        warning "Version TO-BE-DEFINE"
        ;;
      "TBD-2")
        warning "Version TO-BE-DEFINE too"
        ;;
      *)
        error "invalid option $REPLY"
        ;;
    esac
  done
fi
fi

if [ -z "$server_name" ]; then
  while [ -z "${server_name}" ]; do
    read -r -p "Enter a server_name: " server_name
  done
fi

info "Apache version: $apache_version"
info "Creating vhost for $server_name"

prepare_and_cleanup () {
  debug "Upgrading system"
  yum upgrade -y

  debug "Removing old Apache installations"
  yum remove httpd -y
  rm -rf /etc/httpd
}

create_directory(){
  if [ ! -d "$1" ]; then
    debug "Creating directory $1"
    mkdir -p "$1"
  fi
}

configure_apache(){
  info "Configuring Apache"
  create_directory /etc/httpd/sites-enabled
  create_directory /etc/httpd/sites-available

  if ! grep -Fxq "IncludeOptional sites-enabled/*.conf" /etc/httpd/conf/httpd.conf; then
  cat <<EOT >> /etc/httpd/conf/httpd.conf

## Folder for sites-enabled configuration folder
IncludeOptional sites-enabled/*.conf

EOT
  fi

  systemctl restart httpd
}

install_apache(){
  if ! command -v httpd &> /dev/null; then
    debug "Installing Apache $1"
    yum install httpd-"$1" -y

    debug "Enabling Apache"
    systemctl start httpd
    systemctl enable httpd

    configure_apache
  fi
}

## Disclamer, I use 8080, to prove the point of vhost, multiple sites in one server
## Without assigning a domain

create_vhost(){
  info "Create a virtual host"
  cat <<EOT >> /etc/httpd/sites-available/"$1".conf
listen 8080

<VirtualHost *:8080>
  ServerName "$1"
  DocumentRoot /var/www/$1/public_html
  <Directory /var/www/$1/public_html>
    AllowOverride All
    Options FollowSymLinks
  </Directory>
</VirtualHost>
EOT
}

activate_vhost(){
  debug "Activate the virtual host"
  sudo ln -s /etc/httpd/sites-available/"$1".conf /etc/httpd/sites-enabled/"$1".conf
  systemctl reload httpd
}

prepare_and_cleanup
install_apache $apache_version

info "Create a VHost and activate it"

create_vhost "$server_name"
activate_vhost "$server_name"

info "Include a 301 redirect inside the VHost"
cat <<EOT >> /var/www/"$server_name"/public_html/.htaccess
# Include a 301 redirect inside the VHost

Redirect 301 "/old-page" "/new-page"

EOT

info "Include a Forbidden redirect"
cat <<EOT >> /var/www/"$server_name"/public_html/.htaccess
# Include a Forbidden redirect

RewriteCond %{REQUEST_URI} ^/forbiden.html
RewriteRule ^(.*)$ - [F,L]

EOT

info "Include a regex redirect that catches all of the requests containing \"career\" and \".html\" and redirects them to the /careers path"
cat <<EOT >> /var/www/"$server_name"/public_html/.htaccess
# Catches all of the requests containing "career" and ".html" and redirects them to the /careers path

## Remove the .html from the end of the request (This will affect all the url)

RewriteCond %{THE_REQUEST} /([^.]+)\.html [NC]
RewriteRule ^ /%1 [NC,L,R]

RewriteCond %{REQUEST_FILENAME}.html -f
RewriteRule ^ %{REQUEST_URI}.html [NC,L]

## Redirect career to /careers

Redirect 301 "/career" "/careers"

EOT

info "Include a PassThrough redirect"
cat <<EOT >> /var/www/"$server_name"/public_html/.htaccess
# Include a PassThrough redirect
RewriteRule "img/(.+)\.png$" "/files/\$1.png" [PT]

EOT

info "Include Basic Authentication (admin:)"
cat <<EOT >> /var/www/"$server_name"/public_html/.htaccess
# Include Basic Authentication
<Files privates.html>
AuthName "Dialog prompt"
AuthType Basic
AuthUserFile /var/www/$server_name/public_html/.htpasswd
Require valid-user
</Files>

EOT


# chown www-data.www-data /var/www/html/.htaccess

debug "httpd version"
httpd -v
httpd -t
systemctl restart httpd

yum list installed | grep httpd
