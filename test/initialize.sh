#!/usr/bin/env bash

terraform init -backend-config="username=$GITLAB_USER" -backend-config="password=$GITLAB_ACCESS_TOKEN"
