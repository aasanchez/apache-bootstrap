terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/31626790/terraform/state/apache-test"
    lock_address   = "https://gitlab.com/api/v4/projects/31626790/terraform/state/apache-test/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/31626790/terraform/state/apache-test/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  access_key = var.AWS_ACCESS_KEY_ID
  region     = var.AWS_REGION
  secret_key = var.AWS_SECRET_ACCESS_KEY
}

variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_REGION" { default = "eu-central-1" }
variable "AWS_SECRET_ACCESS_KEY" {}
variable "SSH_KEY" {}
