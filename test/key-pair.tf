resource "aws_key_pair" "key" {
  key_name   = "super-vaca-key"
  public_key = var.SSH_KEY
  tags = {
    Name       = "super-vaca-key"
    created_at = timestamp()
    provider   = "terraform"
  }
  lifecycle {
    ignore_changes = [tags["created_at"]]
  }
}
