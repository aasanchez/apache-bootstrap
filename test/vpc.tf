module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "netcentric-vpc"
  cidr = "10.0.0.0/16"

  azs            = ["${var.AWS_REGION}a", "${var.AWS_REGION}b", "${var.AWS_REGION}c"]
  public_subnets = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_ipv6        = true
  enable_nat_gateway = false
  single_nat_gateway = true

  manage_default_security_group = true
  default_security_group_name   = "default netcentric-vpc sg"
  default_security_group_ingress = [
    {
      protocol    = "-1"
      from_port   = "0"
      to_port     = "0"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  default_security_group_egress = [
    {
      protocol    = "-1"
      from_port   = "0"
      to_port     = "0"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  tags = {
    Provider = "terraform"
  }

  vpc_tags = {
    Name = "netcentric-vpc"
  }
}

data "aws_vpc" "netcentric_vpc" {
  depends_on = [module.vpc]
  filter {
    name   = "tag:Name"
    values = ["netcentric-vpc"]
  }
}

data "aws_subnet" "public_1a" {
  depends_on = [module.vpc]
  filter {
    name   = "tag:Name"
    values = ["netcentric-vpc-public-eu-central-1a"]
  }
}

data "aws_security_group" "default" {
  depends_on = [module.vpc]
  filter {
    name   = "tag:Name"
    values = ["default netcentric-vpc sg"]
  }
}
