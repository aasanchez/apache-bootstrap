resource "aws_instance" "centos" {
  ami                         = "ami-04c21037b3f953d37"
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.key.id
  subnet_id                   = data.aws_subnet.public_1a.id
  vpc_security_group_ids      = [data.aws_security_group.default.id]

  tags = {
    Name       = "NetCentric-Test"
    created_at = timestamp()
    provider   = "terraform"
  }

  lifecycle {
    ignore_changes = [tags["created_at"]]
  }

}
